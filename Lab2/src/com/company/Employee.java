package com.company;

public class Employee {
    private String surname;
    private String assignment;
    private int yearOfBirth;
    private int salary;

    public Employee(String surname, String assignment, int yearOfBirth, int salary) {
        this.surname = surname;
        this.assignment = assignment;
        this.yearOfBirth = yearOfBirth;
        this.salary = salary;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
