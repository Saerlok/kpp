package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLOutput;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        List<Employee> employeeList = new ArrayList<Employee>();
        Map<String, List<Employee>> map = new HashMap<String, List<Employee>>();
        Map<String, List<Employee>> salaryMap = new HashMap<String, List<Employee>>();
        Map<String, Employee> mapForTwo = new HashMap<String, Employee>();
        Scanner numToMenu = new Scanner(System.in);
        int num;
        while(true) {
            System.out.println("Welcome\nPress 1 to read data from file\n" +
                    "Press 2 to print map\nPress 3 to sort map\nPress 4 to get min and max salary\n" +
                    "Press 5 to print map with Salary key\nPress 6 to create collection from 2 files\n" +
                    "Press 7 to delete boomers\n" +
                    "Press else number to exit\n"
            );
            num = numToMenu.nextInt();
            if(num == 1) {
                employeeList = FileReader();
                PrintAllList(employeeList);
            }
            else if(num == 2) {
                map = CreateMap(employeeList);
                PrintMap(map);
            }
            else if(num == 3) {
                map.get("admin").sort((Employee o1, Employee o2) -> o1.getYearOfBirth() - o2.getYearOfBirth());
                map.get("cashier").sort((Employee o1, Employee o2) -> o1.getYearOfBirth() - o2.getYearOfBirth());
                map.get("manager").sort((Employee o1, Employee o2) -> o1.getYearOfBirth() - o2.getYearOfBirth());
                PrintMap(map);
            }
            else if(num == 4) {
                FindMaxAndMinSalary(employeeList);
            }
            else if(num == 5) {
                salaryMap = CreateMapSalaryKey(employeeList);
                PrintSalaryMap(salaryMap);
            }
            else if(num == 6) {
                mapForTwo = FileReaderForTwo();
                mapForTwo.forEach((key, value) -> System.out.println(value.getSurname() + "  "
                        + value.getAssignment() + "  " + value.getYearOfBirth() + "  " + value.getSalary()));
            }
            else if(num == 7){
                DeleteBoomersFromMap(mapForTwo, 1994);
            }
            else break;
        }
    }

    public static void PrintMap(Map<String, List<Employee>> map){
        for(int i = 0; i < map.get("cashier").size(); i++) {
            System.out.println(map.get("cashier").get(i).getYearOfBirth() + "  " +
                    map.get("cashier").get(i).getAssignment() + "  " +
                    map.get("cashier").get(i).getSurname() + "  " +
                    map.get("cashier").get(i).getSalary()
            );
        }
        for(int i = 0; i < map.get("manager").size(); i++) {
            System.out.println(map.get("manager").get(i).getYearOfBirth() + "  " +
                    map.get("manager").get(i).getAssignment() + "  " +
                    map.get("manager").get(i).getSurname() + "  " +
                    map.get("manager").get(i).getSalary()
            );
        }
        for(int i = 0; i < map.get("admin").size(); i++) {
            System.out.println(map.get("admin").get(i).getYearOfBirth() + "  " +
                    map.get("admin").get(i).getAssignment() + "  " +
                    map.get("admin").get(i).getSurname() + "  " +
                    map.get("admin").get(i).getSalary()
            );
        }
        System.out.println("");
    }

    public static List<Employee> FileReader(){
        List<Employee> employeeList = new ArrayList<Employee>();
        BufferedReader br = null;
        try {
            /*File file = new File("inputFile2.txt");
            if(!file.exists())
                file.createNewFile();*/
            br = new BufferedReader(new FileReader("inputFile.txt"));
            String tempLine, tempLine2 = new String();
            String surname, assignment;
            int yearOfBirth, salary;
            boolean trigger = false;
            while ((tempLine = br.readLine()) != null){
                String[] words = tempLine.split(",");
                surname = words[0];
                assignment = words[1];
                yearOfBirth = Integer.valueOf(words[2]);
                salary = Integer.valueOf(words[3]);
                employeeList.add(new Employee(surname, assignment, yearOfBirth, salary));
            }
        } catch (IOException e){
            System.out.println("Error" + e);
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return employeeList;
    }

    public static void PrintAllList( List<Employee> employeeList){
        for(int i = 0; i < employeeList.size(); i++){
            System.out.println(employeeList.get(i).getSurname() + "  " +
                    employeeList.get(i).getAssignment() + "  " +
                    employeeList.get(i).getYearOfBirth() + "  " +
                    employeeList.get(i).getSalary()
            );
        }
        System.out.println("");
    }

    public static Map<String, List<Employee>> CreateMap( List<Employee> employeeList){
        Map<String, List<Employee>> map = new HashMap<String, List<Employee>>();
        List<Employee> employeeListMan = new ArrayList<Employee>();
        List<Employee> employeeListCash = new ArrayList<Employee>();
        List<Employee> employeeListAdm = new ArrayList<Employee>();
        for(int i = 0; i < employeeList.size(); i++) {
            if(employeeList.get(i).getAssignment().matches("admin")){
                employeeListAdm.add(employeeList.get(i));
            }
            if(employeeList.get(i).getAssignment().matches("cashier")){
                employeeListCash.add(employeeList.get(i));
            }
            if(employeeList.get(i).getAssignment().matches("manager")){
                employeeListMan.add(employeeList.get(i));
            }
        }
        map.put(employeeListMan.get(0).getAssignment(), employeeListMan);
        map.put(employeeListCash.get(0).getAssignment(), employeeListCash);
        map.put(employeeListAdm.get(0).getAssignment(), employeeListAdm);
        return map;
    }


    public static void FindMaxAndMinSalary(List<Employee> employeeList) {
        employeeList.sort((Employee o1, Employee o2) -> o1.getSalary() - o2.getSalary());
        System.out.println("Min salary is: " + employeeList.get(0).getSalary() + "  max salary is: "  +
                employeeList.get(employeeList.size()-1).getSalary());
        System.out.println("");
    }

    public static Map<String, List<Employee>> CreateMapSalaryKey( List<Employee> employeeList){
        Map<String, List<Employee>> map = new HashMap<String, List<Employee>>();
        List<Employee> employeeListLow = new ArrayList<Employee>();
        List<Employee> employeeListMedium = new ArrayList<Employee>();
        List<Employee> employeeListHigh = new ArrayList<Employee>();
        int low = 2500, medium = 3500;
        for(int i = 0; i < employeeList.size(); i++) {
            if(employeeList.get(i).getSalary() < low){
                employeeListLow.add(employeeList.get(i));
            }
            if(employeeList.get(i).getSalary() < medium && employeeList.get(i).getSalary() > low){
                employeeListMedium.add(employeeList.get(i));
            }
            if(employeeList.get(i).getSalary() > medium){
                employeeListHigh.add(employeeList.get(i));
            }
        }
        map.put("low", employeeListLow);
        map.put("medium", employeeListMedium);
        map.put("high", employeeListHigh);
        return map;
    }

    public static void PrintSalaryMap(Map<String, List<Employee>> map){
        for(int i = 0; i < map.get("low").size(); i++) {
            System.out.println(map.get("low").get(i).getSalary() + "  " +
                    map.get("low").get(i).getAssignment() + "  " +
                    map.get("low").get(i).getYearOfBirth() + "  " +
                    map.get("low").get(i).getSurname()
            );
        }
        for(int i = 0; i < map.get("medium").size(); i++) {
            System.out.println(map.get("medium").get(i).getSalary() + "  " +
                    map.get("medium").get(i).getAssignment() + "  " +
                    map.get("medium").get(i).getYearOfBirth() + "  " +
                    map.get("medium").get(i).getSurname()
            );
        }
        for(int i = 0; i < map.get("high").size(); i++) {
            System.out.println(map.get("high").get(i).getSalary() + "  " +
                    map.get("high").get(i).getAssignment() + "  " +
                    map.get("high").get(i).getYearOfBirth() + "  " +
                    map.get("high").get(i).getSurname()
            );
        }
        System.out.println("");
    }

    public static Map<String, Employee> FileReaderForTwo(){
        Map<String, Employee> map = new HashMap<String, Employee>();
        List<Employee> employeeList = new ArrayList<Employee>();
        BufferedReader br = null;
        BufferedReader br2 = null;
        try {
            br = new BufferedReader(new FileReader("inputFile.txt"));
            br2 = new BufferedReader(new FileReader("inputFile2.txt"));
            String tempLine;
            String surname, assignment;
            int yearOfBirth, salary;

            while ((tempLine = br.readLine()) != null){
                String[] words = tempLine.split(",");
                surname = words[0];
                assignment = words[1];
                yearOfBirth = Integer.valueOf(words[2]);
                salary = Integer.valueOf(words[3]);
                employeeList.add(new Employee(surname, assignment, yearOfBirth, salary));
                map.put(employeeList.get(employeeList.size()-1).getSurname(),
                        employeeList.get(employeeList.size()-1));
            }
            while ((tempLine = br2.readLine()) != null){
                String[] words = tempLine.split(",");
                surname = words[0];
                assignment = words[1];
                yearOfBirth = Integer.valueOf(words[2]);
                salary = Integer.valueOf(words[3]);
                employeeList.add(new Employee(surname, assignment, yearOfBirth, salary));
                map.put(employeeList.get(employeeList.size()-1).getSurname(),
                        employeeList.get(employeeList.size()-1));
            }
        } catch (IOException e){
            System.out.println("Error" + e);
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    public static void DeleteBoomersFromMap(Map<String, Employee> map, int boomerYear){
        Iterator<Map.Entry<String, Employee>> itr = map.entrySet().iterator();

        while(itr.hasNext())
        {
            Map.Entry<String, Employee> entry = itr.next();
            if(entry.getValue().getYearOfBirth() < boomerYear){
                itr.remove();
            }
        }

        map.forEach((key, value) -> System.out.println(value.getSurname() + "  "
                + value.getAssignment() + "  " + value.getYearOfBirth() + "  " + value.getSalary())
        );

    }
}
