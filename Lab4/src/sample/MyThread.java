package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;

public class MyThread extends Thread{
    public Thread t;
    public ObservableList<String> list;
    ListView<String> textArea = null;
    /*@Override
    public void run() {
        Controller.DataToPrint(Controller.someData());
    }*/
    MyThread(String name, ListView<String> tf){

        this.setName(name);
        textArea = tf;
    }

    public void run() {
        list = FXCollections.observableArrayList();
        System.out.println("Running " + getName());
        list.add("Running " + getName());
        try {
            for (int i = 4; i > 0; i--) {
                list.add("Thread: " + getName() + ", " + i);
                System.out.println("Thread: " + getName() + ", " + i);
                // Let the thread sleep for a while.
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            list.add("Thread " + getName() + " interrupted.");
            System.out.println("Thread " + getName() + " interrupted.");
        }
        list.add("Thread " + getName() + " exiting.");
        System.out.println("Thread " + getName() + " exiting.");

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (String i: textArea.getItems()) {
            list.add(i);
        }
        //textArea.setItems(list);
    }

    public void start() {
        System.out.println("Starting " + getName());
        if (t == null) {
            t = new Thread(this, getName());
            t.start();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public MyThread() {
        /*MyThread R1 = new MyThread( "Thread-1");
        R1.start();

        MyThread R2 = new MyThread( "Thread-2");
        R2.start();*/
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
