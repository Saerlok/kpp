package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class Monitor extends Thread{
    List<MyThread> tList;
    ListView<String> textArea = null;
    public ObservableList<String> list;
    ListView<String> textArea2 = null;

    Monitor(List<MyThread> tList, ListView<String> tf, ListView<String> tf2){
        this.textArea = tf;
        this.tList = tList;
        this.textArea2 = tf2;
    }


    @Override
    public void run() {
        //ObservableList<String> list = FXCollections.observableArrayList();
        list = FXCollections.observableArrayList();
        ObservableList<String> list2 = FXCollections.observableArrayList();
        //for(int j = 0; j < tList.size(); j++) {
            System.out.println("Running " + getName());
            list.add("Running " + getName());
            try {
                for (int i = 4; i > 0; i--) {
                    list.add("Thread: " + getName() + ", " + i);
                    System.out.println("Thread: " + getName() + ", " + i);
                    // Let the thread sleep for a while.
                    Thread.sleep(500);
                }
            } catch (InterruptedException e) {
                list.add("Thread " + getName() + " interrupted.");
                System.out.println("Thread " + getName() + " interrupted.");
            }
            list.add("Thread " + getName() + " exiting.");
            System.out.println("Thread " + getName() + " exiting.");

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        //}
        list2.addAll(list);
        for (String i: textArea.getItems()) {
            list.add(i);
        }
        for (String i: textArea2.getItems()) {
            list2.add(i);
        }
        textArea.setItems(list);
        textArea2.setItems(list2);
    }

    /*public void printLog() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {

                        Thread.sleep(100000);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
        thread.start();
    }*/
}
