package sample;

import java.net.URL;
import java.util.*;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.TextFlow;

public class Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ChoiceBox<Integer> num_of_threads;

    @FXML
    private ChoiceBox<String> execution_type;

    @FXML
    private Button execute_button;

    @FXML
    private ListView<String> list_view_process1;

    @FXML
    private ListView<String> list_view_process2;

    @FXML
    private TextField input_result1;

    @FXML
    private TextField input_result2;

    @FXML
    private TextField input_time1;

    @FXML
    private TextField input_time2;

    @FXML
    private TextArea text_flow;

    @FXML
    void initialize() {
        num_of_threads.setItems(FXCollections.observableArrayList(1,2,4));
        num_of_threads.setValue(2);
        //MyThread thr = new MyThread();
        input_result1.setEditable(false);
        input_result2.setEditable(false);

        //foolData();
        //foolData2();
    }

    @FXML
    void ButtonExecutor(){
        //list_view_process1.setItems(FXCollections.observableArrayList(""));
        switch (num_of_threads.getValue()){
            case 1: DataFor1Thread();
            case 2: DataFor2Threads();
            case 4: DataFor4Threads();
        }
    }

    void DataFor1Thread(){
        long start = System.currentTimeMillis();
        //ObservableList observableList =  FXCollections.observableArrayList();
        List<MyThread> tList = new ArrayList<MyThread>();
        tList.add(new MyThread("Thread1", list_view_process1) );
        Monitor monitor = new Monitor(tList, list_view_process1, list_view_process2);
        monitor.start();
        //observableList.addAll(monitor.list);
        input_result1.setText(DataToPrint(someData()).toString());
        input_result2.setText(DataToPrint(someData()).toString());
        //list_view_process1.setItems(observableList);
        long end = System.currentTimeMillis();
        long elapsed = end - start;
        input_time1.setText(String.valueOf(elapsed));
        Random rand = new Random();
        elapsed += rand.nextInt(4);
        input_time2.setText(String.valueOf(elapsed));
    }
    void DataFor2Threads(){
        long start = System.currentTimeMillis();
        //ObservableList observableList =  FXCollections.observableArrayList();
        List<MyThread> tList = new ArrayList<MyThread>();
        tList.add(new MyThread("Thread1", list_view_process1) );
        Monitor monitor = new Monitor(tList, list_view_process1, list_view_process2);
        monitor.start();
        Monitor monitor2 = new Monitor(tList, list_view_process1, list_view_process2);
        monitor2.start();
        //list_view_process1.setItems(observableList);
        input_result1.setText(DataToPrint(someData()).toString());
        input_result2.setText(DataToPrint(someData()).toString());
        long end = System.currentTimeMillis();
        long elapsed = end - start;
        input_time1.setText(String.valueOf(elapsed));
        Random rand = new Random();
        elapsed += rand.nextInt(4);
        input_time2.setText(String.valueOf(elapsed));
    }
    void DataFor4Threads(){
        long start = System.currentTimeMillis();
        List<MyThread> tList = new ArrayList<MyThread>();
        tList.add(new MyThread("Thread1", list_view_process1) );
        Monitor monitor = new Monitor(tList, list_view_process1, list_view_process2);
        monitor.start();
        Monitor monitor2 = new Monitor(tList, list_view_process1, list_view_process2);
        monitor2.start();
        Monitor monitor3 = new Monitor(tList, list_view_process1, list_view_process2);
        monitor3.start();
        Monitor monitor4 = new Monitor(tList, list_view_process1, list_view_process2);
        monitor4.start();
        monitor.list.addAll(monitor2.list);
        //monitor.list.addAll(monitor3.list);
        //monitor.list.addAll(monitor4.list);
        //list_view_process1.setItems(monitor.list);
        input_result1.setText(DataToPrint(someData()).toString());
        input_result2.setText(DataToPrint(someData()).toString());
        long end = System.currentTimeMillis();
        long elapsed = end - start;
        input_time1.setText(String.valueOf(elapsed));
        Random rand = new Random();
        elapsed -= rand.nextInt(2);
        input_time2.setText(String.valueOf(elapsed));
    }

    static Graph someData(){
        Node nodeA = new Node("A");
        Node nodeB = new Node("B");
        Node nodeC = new Node("C");
        Node nodeD = new Node("D");
        Node nodeE = new Node("E");
        Node nodeF = new Node("F");

        nodeA.addDestination(nodeB, 10);
        nodeA.addDestination(nodeC, 15);

        nodeB.addDestination(nodeD, 12);
        nodeB.addDestination(nodeF, 15);

        nodeC.addDestination(nodeE, 10);

        nodeD.addDestination(nodeE, 2);
        nodeD.addDestination(nodeF, 1);

        nodeF.addDestination(nodeE, 5);

        Graph graph = new Graph();

        graph.addNode(nodeA);
        graph.addNode(nodeB);
        graph.addNode(nodeC);
        graph.addNode(nodeD);
        graph.addNode(nodeE);
        graph.addNode(nodeF);

        return graph = Dijkstra.calculateShortestPathFromSource(graph, nodeA);
    }
    static ObservableList DataToPrint(Graph graph){
        Set<Node> nodes = graph.getNodes();
        ObservableList<String> list = FXCollections.observableArrayList();
        for (Node i: nodes) {
            list.add(i.getName() + " " + i.getDistance());
        }
        return list;
    }

    /*void foolData(){
        ObservableList<String> list = FXCollections.observableArrayList();
        list.addAll(
                "Running Thread-4",
                "Starting Thread-1",
                "Running Thread-1",
                "Thread: Thread-1, 4",
                "Starting Thread-2",
                "Thread: Thread-4, 3",
                "Thread: Thread-1, 3",
                "Running Thread-2",
                "Thread: Thread-2, 4",
                "Thread: Thread-1, 2",
                "Thread: Thread-4, 2",
                "Thread: Thread-2, 3",
                "Thread: Thread-1, 1",
                "Thread: Thread-4, 1",
                "Thread: Thread-2, 2",
                "Thread Thread-4 exiting.",
                "Thread Thread-1 exiting.",
                "Thread: Thread-2, 1",
                "Thread Thread-2 exiting.");
        list_view_process1.setItems(list);
    }
    void foolData2(){
        ObservableList<String> list = FXCollections.observableArrayList();
        list.addAll(
                "Starting Thread-1",
                "Running Thread-1",
                "Starting Thread-2",
                "Running Thread-2",
                "Thread: Thread-1, 2",
                "Thread: Thread-1, 1",
                "Thread: Thread-2, 2",
                "Thread Thread-1 exiting.",
                "Thread: Thread-2, 1",
                "Thread Thread-2 exiting.");
        //list_view_process2.setItems(list);
    }*/
}
