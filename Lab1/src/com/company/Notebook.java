package com.company;

public class Notebook extends Goods{
    public String type; //stationary / organization / note for//

    public Notebook(String type, String name, int pageAmount, float price){
        this.type = type;
        this.name = name;
        this.pageAmount = pageAmount;
        this.price = price;
        category = "Notebook";
    }
}
