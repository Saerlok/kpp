package com.company;

import java.util.List;

public class Book extends Goods{
    public String autor;
    public String publication;
    public List<Genre> genre;

    public Genre getGenre(int genreNum) {
        return genre.get(genreNum);
    }
    public Book(String autor, String publication, List<Genre> genre, String name,
                int pageAmount, float price){
        this.autor = autor;
        this.publication = publication;
        this.genre = genre;
        this.name = name;
        this.pageAmount = pageAmount;
        this.price = price;
        category = "Book";
    }
}
