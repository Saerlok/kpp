package com.company;
import java.util.ArrayList;
import java.util.List;

public class Basket {
    public List<Goods> goodsToBuy = new ArrayList<Goods>();

    public void addToBasket(Goods item){
        goodsToBuy.add(item);
    }
    public void removeFromBasket(Goods item){
        for(int i = 0; i < goodsToBuy.size(); i++){
            if(goodsToBuy.get(i) == item){
                goodsToBuy.remove(item);
            }
        }
    }
}
