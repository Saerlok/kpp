package com.company;

public class Cashier {
    public Basket totalGoods;

    public Cashier(Basket totalGoods) {
        this.totalGoods = totalGoods;
    }

    public float calculateTotalPrice(){
        float totalPrice = 0;
        for (int i = 0; i < totalGoods.goodsToBuy.size(); i++){
            totalPrice += totalGoods.goodsToBuy.get(i).price;
        }
        return totalPrice;
    }
}
