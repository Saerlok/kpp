package com.company;
import java.util.ArrayList;
import java.util.List;
import java.util.Comparator;
import java.util.Collections;

public class GoodsManager {
    public List<Goods> goods;
    public List<Book> books;

    public GoodsManager(List<Goods> goods) {
        this.goods = goods;
    }

    public void sortBookByGenre(){
        books = new ArrayList<Book>();
        for(int i = 0; i < goods.size(); i++){
            if(goods.get(i).category == "Book"){
                books.add((Book)goods.get(i));
            }
        }
        Genre[] gen = Genre.values();

        for(int i = 0; i < gen.length; i++){
            for(int j = 0; j < books.size(); j++){
                for(int k = 0; k < books.get(j).genre.size(); k++) {
                    if (gen[i] == books.get(j).getGenre(k)) {
                        System.out.println(books.get(j).name + "  " + gen[i]);
                    }
                }
            }
        }
    }

    public List<Goods> sortGoodsByPrice(){
        List<Goods> nList = goods;
        Collections.sort(nList, (o1, o2) -> (int)o1.price - (int)o2.price);
        return nList;
    }

    public List<Goods> sortGoodsByPageAmount(){
        List<Goods> nList = goods;
        Collections.sort(goods, new Comparator<Goods>() { //inner class//
            @Override
            public int compare(Goods o1, Goods o2) {
                return o1.pageAmount - o2.pageAmount;
            }
        });
        return nList;
    }

    public List<Goods> reverseSortGoodsByPageAmount(){
        List<Goods> nList = goods;
        Collections.sort(goods, new reversePageComparator());
        return nList;
    }

    public static class reversePageComparator implements Comparator<Goods> {
        @Override
        public int compare(Goods o1, Goods o2) {
            return o2.pageAmount - o1.pageAmount;
        }
    }

    public List<Goods> reverseSortGoodsByPrice(){
        List<Goods> nList = goods;
        Collections.sort(nList, (o1, o2) -> (int)o2.price - (int)o1.price);
        return nList;
    }

    public void printGoods(List<Goods> goods){
        for(int i = 0; i < goods.size(); i++){
            System.out.println(goods.get(i).category + "  " + goods.get(i).name + "  "
                    + "  " + goods.get(i).pageAmount + "  " + goods.get(i).price);
        }
    }
}

