package com.company;
import java.util.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        GoodsManager man = new GoodsManager(createGoodsList());
        man.printGoods(man.goods);
        int numToMenu = 0, numToSimulation;
        Basket basket = new Basket();
        Scanner scanner = new Scanner(System.in);
        while(true){
            System.out.println("What to do?....");
            System.out.println("Press 1 to sort books by genre");
            System.out.println("Press 2 to sort goods by price");
            System.out.println("Press 3 to reverse sort goods by price");
            System.out.println("Press 4 to sort goods by page amount");
            System.out.println("Press 5 to reverse sort goods by page amount");
            System.out.println("Press 6 to enter simulation");
            System.out.println("Press else to exit");

            numToMenu = scanner.nextInt();
            System.out.println("So you have chosen ... " + numToMenu);
            if(numToMenu == 1){
                man.sortBookByGenre();
            }else if (numToMenu == 2){
                man.printGoods(man.sortGoodsByPrice());
            }else if (numToMenu == 3){
                man.printGoods(man.reverseSortGoodsByPrice());
            }else if (numToMenu == 4){
                man.printGoods(man.sortGoodsByPageAmount());
            }else if (numToMenu == 5) {
                man.printGoods(man.reverseSortGoodsByPageAmount());
            }else if (numToMenu == 6){
                System.out.println("Welcome to the greatest simulation, choose one item from the list");
                while (true) {
                    System.out.println("Press 0 to buy your goods");
                    man.printGoods(man.goods);
                    numToSimulation = scanner.nextInt();
                    basket.addToBasket(man.goods.get(numToSimulation + 1));
                    if(numToSimulation == 0) break;
                }
                Cashier cashier = new Cashier(basket);
                System.out.println("Total price for your goods: " + cashier.calculateTotalPrice());
            }else break;
        }
    }

    public static List<Goods> createGoodsList(){
        List<Goods> listOfGoods = new ArrayList<Goods>();

        //BOOKS//
        List<Genre> listOfGenre = new ArrayList<Genre>();
        listOfGenre.add(Genre.Biography);
        listOfGenre.add(Genre.Drama);
        listOfGenre.add(Genre.Mystery);
        Book book = new Book("F. Scott Fitzgerald", "Lviv", listOfGenre,
                "The Great Gatsby", 415, 120);
        listOfGoods.add(book);
        listOfGenre = new ArrayList<Genre>();

        listOfGenre.add(Genre.Mythology);
        listOfGenre.add(Genre.Poetry);
        book = new Book("Homer", "Kyiv", listOfGenre, "The Odyssey",
                712, 210);
        listOfGoods.add(book);
        listOfGenre = new ArrayList<Genre>();

        listOfGenre.add(Genre.Mystery);
        listOfGenre.add(Genre.Drama);
        listOfGenre.add(Genre.Mythology);
        book = new Book("Dante Alighieri", "Kyiv", listOfGenre, "The Divine Comedy",
                489, 178);
        listOfGoods.add(book);
        listOfGenre = new ArrayList<Genre>();

        listOfGenre.add(Genre.Drama);
        book = new Book("Ralph Ellison", "Chernivtsi", listOfGenre, "Invisible Man",
                366, 310);
        listOfGoods.add(book);
        listOfGenre = new ArrayList<Genre>();

        listOfGenre.add(Genre.Poetry);
        listOfGenre.add(Genre.Short_Story);
        listOfGenre.add(Genre.Mythology);
        listOfGenre.add(Genre.Fable);
        book = new Book("India/Iran/Iraq/Egypt", "Lviv", listOfGenre,
                "One Thousand and One Nights", 558, 130);
        listOfGoods.add(book);
        listOfGenre = new ArrayList<Genre>();

        listOfGenre.add(Genre.Poetry);
        listOfGenre.add(Genre.Mythology);
        listOfGenre.add(Genre.Mystery);
        book = new Book("Virgil", "Kyiv", listOfGenre, "The Aeneid",
                710, 280);
        listOfGoods.add(book);

        //NOTEBOOKS//
        Notebook notebook = new Notebook("Stationary", "Agenzio", 80, 50);
        listOfGoods.add(notebook);

        notebook = new Notebook("Note For", "Silence", 150, 30);
        listOfGoods.add(notebook);

        notebook = new Notebook("Note For", "Agenzio", 150, 45);
        listOfGoods.add(notebook);

        notebook = new Notebook("Organization", "Agenzio", 60, 120);
        listOfGoods.add(notebook);

        notebook = new Notebook("Stationary", "Silence", 100, 130);
        listOfGoods.add(notebook);

        //CALENDARS//
        Celendar calendar = new Celendar(2020, "Zen", 1, 30);
        listOfGoods.add(calendar);

        calendar = new Celendar(2021, "Agenzio", 12, 90);
        listOfGoods.add(calendar);

        calendar = new Celendar(2020, "Today", 1, 50);
        listOfGoods.add(calendar);

        calendar = new Celendar(2021, "Zen", 12, 120);
        listOfGoods.add(calendar);

        calendar = new Celendar(2021, "Zen", 12, 60);
        listOfGoods.add(calendar);

        return listOfGoods;
    }
}
