package com.company;

import java.util.List;

public enum Genre {
    Drama,
    Fantasy,
    Fable,
    Horror,
    Mystery,
    Poetry,
    Mythology,
    Biography,
    Autobiography,
    Science_Fiction,
    Short_Story,
    Fiction;

}
