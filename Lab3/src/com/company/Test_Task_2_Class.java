package com.company;

import org.junit.Assert;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test_Task_2_Class {
    @Test
    public void TestFileReading(){
        var T2 = new Task_2_Class("FileWithLinesQ2.txt");
        Assert.assertEquals("This narrative begins with the death of its hero.",
                T2.getStringsFromFile().get(0));
    }

    @Test
    public void TestTask2(){
        var T2 = new Task_2_Class("FileWithLinesQ2.txt");
        T2.DeleteWordsFromText();
        boolean testIs = false;
        Pattern pattern1 = Pattern.compile ("\\Won\\W|\\Win\\W|\\Wout\\W|\\W[a]\\W|\\Wthe\\W|\\Wor\\W|\\Ware\\W");
        for(int i = 0; i < T2.getStringsFromFile().size(); i++){
            Matcher matcher1 = pattern1.matcher (T2.getStringsFromFile().get(i));
            while (matcher1.find()) {
                testIs = true;
            }
        }
        Assert.assertEquals(false, testIs);
    }
}
