package com.company;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner nToMenu = new Scanner(System.in);
        int numToMenu;
        while(true) {
            System.out.println("Press 1 to do in Eng\nPress 2 to do in Ukr\nPress 3 to do 2nd task\n" +
                    "Else to exit\n");
            numToMenu = nToMenu.nextInt();
            if(numToMenu == 1) {
                List<String> stringsFromFile = new ArrayList<String>();
                stringsFromFile = ReadFromFile("FileWithLines.txt");
                for (int i = 0; i < stringsFromFile.size(); i++) {
                    System.out.println(stringsFromFile.get(i));
                }
                Find(stringsFromFile);
            }
            else if(numToMenu == 2) {
                List<String> stringsFromFile2 = new ArrayList<String>();
                stringsFromFile2 = ReadFromFile("FileWithLines2.txt");
                for (int i = 0; i < stringsFromFile2.size(); i++) {
                    System.out.println(stringsFromFile2.get(i));
                }
                Find(stringsFromFile2);
            }
            else if(numToMenu == 3) {
                var T2 = new Task_2_Class("FileWithLinesQ2.txt");
                T2.PrintSource();
                System.out.println("");
                T2.DeleteWordsFromText();
                /*for (int i = 0; i < T2.getStringsFromFile().size(); i++) {
                    System.out.println(T2.getStringsFromFile().get(i));
                }*/
                T2.PrintSource();
            }
            else break;
        }
    }

    public static List<String> ReadFromFile( String filename){
        BufferedReader buff = null;
        List<String> stringsFromFile = new ArrayList<String>();
        try {
            buff = new BufferedReader(new FileReader(filename));
            String lineFromFile;
            while((lineFromFile = buff.readLine()) != null){
                stringsFromFile.add(lineFromFile);
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        finally {
            try{
                buff.close();
            }
            catch (IOException ex){
                System.out.println(ex.getMessage());
            }
        }
        return stringsFromFile;
    }

    public static void Find(List<String> stringsFromFile){
        Pattern pattern1 = Pattern.compile ("\\([^()]*\\)");
        Pattern pattern2 = Pattern.compile ("\\([^()]*[()][^()]*[()][^()]*\\)");
        String str = new String();
        for(int i = 0; i < stringsFromFile.size(); i++){
            str += stringsFromFile.get(i);
        }
        Matcher matcher1 = pattern1.matcher (str);
        System.out.println("");
        while (matcher1.find()) {
            String nStr = matcher1.group().replaceAll("\\d", "#");
            System.out.println(matcher1.start() + "  " + nStr + "  ");
        }
        Matcher matcher2 = pattern2.matcher (str);
        while (matcher2.find()) {
            String nStr = matcher2.group().replaceAll("\\d", "#");
            System.out.println(matcher2.start() + "  " + nStr + "  ");
        }
        //System.out.println(str);
    }


}
