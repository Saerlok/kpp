package com.company;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task_2_Class {
    private List<String> stringsFromFile;
    public List<String> getStringsFromFile() {
        return stringsFromFile;
    }
    public void setStringsFromFile(List<String> stringsFromFile) {
        this.stringsFromFile = stringsFromFile;
    }

    public Task_2_Class(String fileName) {
        BufferedReader buff = null;
        this.stringsFromFile = new ArrayList<String>();
        try {
            buff = new BufferedReader(new FileReader(fileName));
            String lineFromFile;
            while((lineFromFile = buff.readLine()) != null){
                stringsFromFile.add(lineFromFile);
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        finally {
            try{
                buff.close();
            }
            catch (IOException ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    public void DeleteWordsFromText(){
        String buff = new String();
        for(int i = 0; i < stringsFromFile.size(); i++) {
            buff = stringsFromFile.get(i).replaceAll("\\W[a]\\W|\\W*the\\W|\\Wor\\W|\\Ware\\W"," ");
            buff = buff.replaceAll("\\Won\\W|\\Win\\W|\\Wout\\W"," ");
            stringsFromFile.remove(i);
            stringsFromFile.add(i, buff);
        }
    }
    public void PrintSource(){
        for(int i = 0; i < stringsFromFile.size(); i++){
            System.out.println(stringsFromFile.get(i));
        }
    }
}
