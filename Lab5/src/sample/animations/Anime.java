package sample.animations;

import javafx.animation.TranslateTransition;
import javafx.scene.Node;
import javafx.scene.transform.Translate;
import javafx.util.Duration;

public class Anime {
    // ce zhiznb
    private TranslateTransition transition;

    public Anime(Node node){
        transition = new TranslateTransition(Duration.millis(1000), node);
        transition.setFromX(0f);
        transition.setFromY(0f);
        //transition.setByX(300);
        transition.setByY(-200);
        transition.setCycleCount(40);
        transition.setAutoReverse(true);
    }

    public Anime(Node node, int hui){
        transition = new TranslateTransition(Duration.millis(500), node);
        transition.setFromX(0f);
        transition.setFromY(0f);
        //transition.setByX(300);
        transition.setByY(-200);
        transition.setCycleCount(40);
        transition.setAutoReverse(true);
    }

    public Anime(Node node, int hui, int hui2){
        transition = new TranslateTransition(Duration.millis(700), node);
        transition.setFromX(0f);
        transition.setFromY(0f);
        transition.setByX(-400);
        transition.setCycleCount(30);
        transition.setAutoReverse(true);
    }

    public void PlayAnim(){
        transition.playFromStart();
    }
}
